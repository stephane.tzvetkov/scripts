#!/bin/bash

if [ $# -ne 1 ]
then
    echo "Error - Illegal number of parameters: you need one parameter to specify a file or folder!"
    exit 1
fi

if [ ! -f "$1" ] && [ ! -d "$1" ]
then
	echo "Error - File or folder: $1 does not exist!"
	exit 1
fi

f="$1"

#fmt=$(echo $f | sed 's/[:lower:][:upper:][:lower:]/_&/g' | tr '[A-Z]' '[a-z]' | tr -s " " | tr -s ' ' '_')

# sed 's/|^^*$/|^/'

#$pattern = '[^a-zA-Z]'
#$string -replace $pattern, '_'

# independent letters and ligatures:
#fmt=$(echo $f\
#								| tr 'Æ' 'ae'| tr 'æ' 'ae'\
#								| tr 'Ɑ' 'a' | tr 'ɑ' 'a' \
#								| tr 'Ꞵ' 'b' | tr 'ꞵ' 'b' \
#								| tr 'Ð' 'd' | tr 'ð' 'd' \
#								| tr 'Ǝ' 'e' | tr 'ǝ' 'e' \
#								| tr 'Ə' 'e' | tr 'ə' 'e' \
#								| tr 'Ɛ' 'e' | tr 'ɛ' 'e' \
#								| tr 'Ɣ' 'g' | tr 'ɣ' 'g' \
#								| tr 'İ' 'i' | tr 'i' 'i' \
#								| tr 'Ɩ' 'i' | tr 'ɩ' 'i' \
#								| tr 'Ŋ' 'n' | tr 'ŋ' 'n' \
#								| tr 'Œ' 'oe'| tr 'œ' 'oe'\
#								| tr 'Ɔ' 'dz'| tr 'ɔ' 'dz'\
#								| tr 'Ꞷ' 'w' | tr 'ꞷ' 'w' \
#								| tr 'Ʊ' 'u' | tr 'ʊ' 'u' \
#								| tr 'K' 'k' | tr 'ĸ' 'k' \
#								| tr 'ẞ' 'ss'| tr 'ß' 'ss'\
#								| tr 'Ʃ' 's' | tr 'ʃ' 's' \
#								| tr 'Þ' 'th'| tr 'þ' 'th'\
#								| tr 'Ʋ' 'v' | tr 'ʋ' 'v' \
#								| tr 'Ƿ' 'w' | tr 'ƿ' 'w' \
#								| tr 'Ȝ' 'g' | tr 'ȝ' 'g' \
#								| tr 'Ʒ' 'z' | tr 'ʒ' 'z' )

#https://www.computerhope.com/unix/utr.htm
# full greek alphabet:

fmt=$(echo $f\
								| iconv -f UTF-8 -t ASCII//TRANSLIT | tr '[A-Z]' '[a-z]' | tr '[:blank:]' '_' |  tr -cd '[:alnum:]_#' | tr -s "_" | tr -s "#")

								#https://en.wikipedia.org/wiki/List_of_Latin-script_alphabets
								#https://en.wikipedia.org/wiki/Greek_alphabet

if [ -f $fmt ] || [ -d $fmt ]
then
	echo "Error - File or folder: $fmt already exists!"
	exit 3
fi

mv "$f" $fmt
