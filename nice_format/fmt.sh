#!/bin/bash

if [[ $# != 1 ]]
then
    echo "Error - Illegal number of parameters: you need one parameter to specify a file or folder!"
    exit 1
fi

if [[ ! -f $1 && ! -d $1 ]]
then
	echo "Error - File or folder: $1 does not exist!"
	exit 1
fi

f=$1
f_ext=""

if [[ $1 =~ "." ]]
then
	f_ext=$(echo $f | rev | cut -d "." -f1 | rev)

	if [[ $1 =~ ".tar." ]]
	then
		f_tar_ext=$(echo $f | rev | cut -d "." -f2 | rev)

		if [[ $f_tar_ext == "tar" && ! -z "$f_ext" ]]
		then
			f_ext="$f_tar_ext.$f_ext"
		fi
	fi

	f=${f%".$f_ext"}
fi

fmt=$(echo $f | iconv -f UTF-8 -t ASCII//TRANSLIT | tr '[A-Z]' '[a-z]' | tr '[:blank:]' '_' | tr -c '[:alnum:]' '_' | tr -s "_" )
fmt=${fmt%"_"}

if [[ -f $fmt || -d $fmt ]]
then
	echo "Error - File or folder $fmt already exists : no mv $f $fmt"
	exit 3
fi

fmt="$fmt.$f_ext"

mv "$1" $fmt

