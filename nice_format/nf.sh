#!/bin/zsh

# references:
# https://stackoverflow.com/questions/192249/how-do-i-parse-command-line-arguments-in-bash#192266
# https://gist.github.com/bobpaul/ecd74cdf7681516703f20726431eaceb

# initialize params variables
DEBUG=0
FORCE=0
HELP=0
RECURSIVE=0
VERBOSE=0

OUTPUT=-

declare -a EXTRA_NON_OPT_ARGS
DISCARD_EXTRA_NON_OPT_ARGS=0
# 1=Discard, 0=Save opts after -- to ${EXTRA_NON_OPT_ARGS}

# bash strict mode (saner programming env):
# http://redsymbol.net/articles/unofficial-bash-strict-mode/
# https://www.davidpashley.com/articles/writing-robust-shell-scripts/

# exit if any command has a non-zero exit status:
set -o errexit
# same a "set -e"

# exit if a reference to any variable haven't been previously defined:
# (with the exceptions of $* and $@)
set -o nounset
# same as "set -u"

# prevents errors in a pipeline from being masked by returning its error code:
set -o pipefail

# prevents from overwriting existing files with the > operator
set -o noclobber

function getopt_check
{
	! getopt --test > /dev/null
	if [[ ${pipestatus[1]} -ne 4 ]]
	then
	    echo "Error - `getopt --test` failed in this environment"
		echo "        GNU's enhanced getopt is required to run this script"
	    exit 1
	fi
}

function stdout_check
{
	if [[ ${pipestatus[1]} -ne 0 ]]
	then
	    # e.g. return value is 1
		echo "Error - getop complains about wrong arguments to stdout"
	    exit 2
	fi
}

function nonopt_check
{
	# handle non-option arguments
	if [[ ${#EXTRA_NON_OPT_ARGS[@]} -ne 1 ]]
	then
		echo "Error - Illegal number of parameters:"
		echo "        you need one parameter to specify a file or folder!"
	    exit 3
	fi
}

function options
{
	getopt_check
	stdout_check

	# An opt followed by a single colon ':' means that it *needs* an argument
	# An opt followed by double colons '::' means that its argument is optional
	OPTS=dfho:rv
	LONGOPTS=debug,force,help,output:,recursive,verbose

	# -use ! and PIPESTATUS to get exit code with errexit set
	# -temporarily store output to be able to check for errors
	# -activate quoting/enhanced mode (e.g. by writing out “--options”)
	# -pass arguments only via   -- "$@"   to separate them correctly
	# - getopt auto adds "--" at the end of ${PARSED}, wich is later set to
	#   "$@" using the set command
	! PARSED=$(getopt --options=$OPTS \
					  --longoptions=$LONGOPTS \
					  --name "$0" \
					  -- "$@")

	# use eval with "$PARSED" to properly handle the quoting
	# the set command sets the list of arguments equal to ${PARSED}
	eval set -- "$PARSED"

	dashestag=0 # flag to track "--" is parsed
	while [[ $# -gt 0 ]]
	do
	    case "$1" in
	        -d|--debug)
	            DEBUG=1
	            ;;
	        -f|--force)
	            FORCE=1
	            ;;
			-h|--help)
				HELP=1
				;;
			-o|--output)
				shift
				OUTPUT="$1"
				;;
			-r|--recursive)
				RECURSIVE=1
				;;
	        -v|--verbose)
	            VERBOSE=1
	            ;;
	        --)
				dashestag=1
				if [[ ${DISCARD_EXTRA_NON_OPT_ARGS} -eq 1 ]]
				then
					break
				fi
	            ;;
	        *)
				if [[ $dashestag -eq 1 ]]
				then
					EXTRA_NON_OPT_ARGS+=("$1");
				fi
	            ;;
	    esac
		shift # expose the next argument
	done

	nonopt_check
}

function check_exist
{
	if [[ ! -f $1 && ! -d $1 ]]
	then
		echo "Error - File or folder: $1 does not exist!"
		exit 4
	fi
}

function format
{
	basename=$(basename "$1")
	dir=$(dirname "$1")
	basename_ext=""

	is_dotfile=0
	if [[ ${basename:0:1} = "." ]]
	then
		is_dotfile=1
	fi

	# handling file extension
	if [[ $1 =~ "." && $is_dotfile == 0 ]]
	then
		basename_ext=$(echo $basename | rev | cut -d "." -f1 | rev)

		if [[ $1 =~ ".tar." ]]
		then
			basename_tar_ext=$(echo $basename | rev | cut -d "." -f2 | rev)

			if [[ $basename_tar_ext == "tar" && ! -z "$basename_ext" ]]
			then
				basename_ext="$basename_tar_ext.$basename_ext"
			fi
		fi
		basename=${basename%".$basename_ext"}
	fi

	# formating
	basename_fmt=$(echo $basename	| iconv -f UTF-8 -t ASCII//TRANSLIT \
									| tr '[A-Z]' '[a-z]' \
									| tr '[:blank:]' '_' \
									| tr -c '[:alnum:]' '_' \
									| tr -s "_" )

	basename_fmt=${basename_fmt%"_"} # remove last  char if it's "_"
	basename_fmt=${basename_fmt#"_"} # remove first char if it's "_"

	# if not already existing : replacing name with mv
	if   [[ "$is_dotfile" -eq 0 && (-f $dir/$basename_fmt  || -d $dir/$basename_fmt)  ]]
	then
		echo "Error - File or folder $dir/$basename_fmt already exists :"
		echo "        no mv $dir/$basename $dir/$basename_fmt"

	elif [[ "$is_dotfile" -eq 1 && (-f $dir/.$basename_fmt || -d $dir/.$basename_fmt) ]]
	then
		echo "Error - File or folder $dir/.$basename_fmt already exists :"
		echo "        no mv $dir/.$basename $dir/.$basename_fmt"
	else
		if [[ "$is_dotfile" -eq 1 ]]
		then
			basename_fmt=".$basename_fmt"

		elif [[ ! -z "$basename_ext" ]]
		then
			basename_fmt="$basename_fmt.$basename_ext"
		else
			basename_fmt="$basename_fmt"
		fi

		if [[ "$1" != "$dir/$basename_fmt" ]]
		then
			mv "$1" $dir/$basename_fmt
		fi
	fi
}

function main
{
	options "$@"

	if [[ $RECURSIVE -eq 1 ]]
	then
		findarray=()
		while IFS=  read -r -d $'\0'
		do
			findarray+=("$REPLY")
		done < <(find "${EXTRA_NON_OPT_ARGS[0]}" -print0)

		for (( idx=${#findarray[@]}-1 ; idx>=0 ; idx-- ))
		do
			format "${findarray[idx]}"
		done
	else
		format "${EXTRA_NON_OPT_ARGS[0]}"
	fi
}

main "$@"

