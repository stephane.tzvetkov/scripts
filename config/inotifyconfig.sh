#!/bin/sh

# The purpose of this script is to synchronize some configuration files.
# The names of those files must be listed in the `INOTIFYPATHS` file.

syncplace="$HOME/config"
inotifyconfig_fromfile="$syncplace/INOTIFYPATHS"
inotifyconfig_tmplog="/tmp/inotifyconfig_tmplog"
inotifyconfig_cache="/tmp/inotifyconfig_cache"
# See `INOTIFYPATHS-examples` for an example of how to create your `INOTIFYPATHS` file. It simply
# contains a list of file paths, on per line, to watch. See `$ man inotifywait` (--fromfile option)

# reload the $inotifyconfig_cache cache file
if [ -f "$inotifyconfig_cache" ]; then
    rm "$inotifyconfig_cache"
    touch "$inotifyconfig_cache"
fi

# reload the $inotifyconfig_tmplog log file
if [ -f "$inotifyconfig_tmplog" ]; then
    rm "$inotifyconfig_tmplog"
    touch "$inotifyconfig_tmplog"
fi

# make sure this script isn't already running
for pid in $(pgrep inotifywait); do
    foundcommand="$(ps -p "$pid" -o args --no-headers)"
    realcommand="inotifywait --format %w -e move_self -o $inotifyconfig_cache --fromfile $inotifyconfig_fromfile"

    if [ "$foundcommand" = "$realcommand" ]; then
        error="Error: this script is already running (process id: $pid)!"
        echo "$error" >> "$inotifyconfig_tmplog"
        echo "$error"
        if [ notify-send ]; then sleep 2 && notify-send -u critical "inotifyconfig" "$error"; fi&
        return 1
    fi
done

# check the file paths of the INOTIFYPATHS file
#known_inotifyconfig_fromfile=""
while IFS="" read -r line || [ -n "$line" ]; do
    #known_inotifyconfig_fromfile="$known_inotifyconfig_fromfile\n$line"

    if [ ! -f "$line" ]; then
        error="Error: in your INOTIFYPATHS, $line is not a file or does not exist!"
        echo "$error" >> "$inotifyconfig_tmplog"
        echo "$error"
        if [ notify-send ]; then sleep 2 && notify-send -u critical "inotifyconfig" "$error"; fi&
        return 2
    fi
    if [ ! -r "$line" ]; then
        error="Error: in your INOTIFYPATHS, $line cannot be read!"
        echo "$error" >> "$inotifyconfig_tmplog"
        echo "$error"
        if [ notify-send ]; then sleep 2 && notify-send -u critical "inotifyconfig" "$error"; fi&
        return 3
    fi

done < "$inotifyconfig_fromfile"
#echo "$known_inotifyconfig_fromfile"

## iterate and print through all files of the $syncplace except for .gitignore and .git/ files
#find "$syncplace" -type f | while read filename; do
#    if [ ! $(echo "$filename" | grep "^$syncplace/.git") ]; then
#        echo $filename
#    fi
#done

# contains(string, substring)
#
# https://stackoverflow.com/a/8811800
#
# Returns 0 if the specified $string contains the specified $substring, otherwise returns 1.
contains() {
    string="$1"
    substring="$2"
    if test "${string#*$substring}" != "$string"
    then
        # $substring is in $string
        return 0
    else
        # $substring is not in $string
        return 1
    fi
}

# inoticopy(modifilepath)
#
# Copy the file in the specified $modifilepath to the copy place after a few operations.
inoticopy() {
    modifilepath="$1"

    # replace first occurence of $USER by "user" in $copypath
    copypath="$(echo "$modifilepath" | sed "s/$USER/user/")"

    # remove de first char (i.e. "/")
    copypath="$(echo "$copypath" | sed "s/^.//")" # remove first char

    # extract the file name of $copypath
    copyfile="$(basename "$copypath")"

    # remove last occurence of $copyfile to extract folder path
    copyfolder="$(echo "$copypath" | sed "s/\(.*\)$copyfile/\1/")"

    # sync file
    mkdir -p "$syncplace/$copyfolder"
    cp "$modifilepath" "$syncplace/$copyfolder$copyfile"
}

# watch files in INOTIFYPATHS
while inotifywait --format %w -e move_self -o "$inotifyconfig_cache" --fromfile "$inotifyconfig_fromfile"; do

    # extract last inotify message containing the last modified file path
    modifilepath="$(tail -n1 "$inotifyconfig_cache")"

    # if inotifyconfig_fromfile has changed, then stop the current script and restart it
    if contains "$modifilepath" "INOTIFYPATHS"; then
        scriptlocation=$(readlink -f "$0")
        exec "$scriptlocation" # stop and restart
    fi

    # copy the file or the modifications made to a file
    inoticopy "$modifilepath"
done

######################
# INOTIFYPATHS-example
######################
# /paht/to/this/file/INOTIFYPATHS
#
# /etc/fstab
#
# /home/username/.config/user-dirs.dirs
# /home/username/.config/user-dirs.locale
# /home/username/.config/fontconfig/fonts.conf
# /home/username/.config/gnupg/gpg.conf
# /home/username/.config/gnupg/gpg-agent.conf
# /home/username/.config/neomutt/neomuttrc
# /home/username/.config/nvim/init.vim
# /home/username/.config/zathura/zathurarc
# /home/username/.config/pulse/client.conf
# /home/username/.config/redshift/redshift.conf
# /home/username/.config/rtorrent/rtorrent.rc
# /home/username/.config/zdotdir/.zimrc
# /home/username/.config/zdotdir/.zlogin
# /home/username/.config/zdotdir/.zlogout
# /home/username/.config/zdotdir/.zprofile
# /home/username/.config/zdotdir/.zshrc
# /home/username/.config/git/config
#
# /home/username/.xinitrc
# /home/username/.zshenv
#
# /usr/src/linux/.config

