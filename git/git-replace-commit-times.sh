#!/bin/sh

# This script will replace every commit times (GIT_AUTHOR_DATE and GIT_COMMITTER_DATE) of your git
# repo to midnight.

# After running this script, you will have to run `$ git push -f origin master` to override
# previous commit times.

# Troubleshooting
# - if you encounter the following error: "remote: GitLab: You are not allowed to force push code
# to a protected branch on this project.", cf.
# https://stackoverflow.com/questions/32246503/fix-gitlab-error-you-are-not-allowed-to-push-code-to-protected-branches-on-thi

# It's also possible to modify commit times when committing (cf.
# https://stackoverflow.com/a/9701130):
# `$ LC_ALL=C TZ='Europe/Paris' GIT_COMMITTER_DATE="$(date +"%a, %d %b %Y 00:00:00 %z")" git commit --date "$(date +"%a, %d %b %Y 00:00:00 %z")"`

git filter-branch -f --env-filter \
    '
    export LC_ALL=C # change local language to english

    echo "\n"
    echo "* GIT_COMMIT = $GIT_COMMIT"

    ###########################################################################

    echo "\n"
    echo "* GIT_AUTHOR_DATE = $GIT_AUTHOR_DATE"

    GIT_AUTHOR_UNIX_DATE=${GIT_AUTHOR_DATE% +*}
    echo "* GIT_AUTHOR_UNIX_DATE = $GIT_AUTHOR_UNIX_DATE"

    GIT_AUTHOR_TIME_ZONE=${GIT_AUTHOR_DATE#* }
    # If the first char of the GIT_AUTHOR_TIME_ZONE is "@",
    # it means that no time zone has been parsed. In this case the default will be +0000.
    FIRST_CHAR="$(echo $GIT_AUTHOR_TIME_ZONE | cut -c1-1)"
    if [ "$FIRST_CHAR" = "@" ]; then
        GIT_AUTHOR_TIME_ZONE="+0000"
    fi
    echo "* GIT_AUTHOR_TIME_ZONE = $GIT_AUTHOR_TIME_ZONE"

    #WRONG_RFC_5322_DATE=$(date -d $GIT_AUTHOR_UNIX_DATE -R)
    #echo "* WRONG_RFC_5322_DATE = $WRONG_RFC_5322_DATE"

    RIGHT_RFC_5322_DATE=$(date -d $GIT_AUTHOR_UNIX_DATE +"%a, %d %b %Y")" 00:00:00 $GIT_AUTHOR_TIME_ZONE"
    echo "* RIGHT_RFC_5322_DATE = $RIGHT_RFC_5322_DATE"

    export GIT_AUTHOR_DATE="$RIGHT_RFC_5322_DATE"

    echo "\n"
    echo "-> new GIT_AUTHOR_DATE = $RIGHT_RFC_5322_DATE"

    ###########################################################################

    echo "\n"
    echo "* GIT_COMMITTER_DATE = $GIT_COMMITTER_DATE"

    GIT_COMMITTER_UNIX_DATE=${GIT_COMMITTER_DATE% +*}
    echo "* GIT_COMMITTER_UNIX_DATE = $GIT_COMMITTER_UNIX_DATE"

    GIT_COMMITTER_TIME_ZONE=${GIT_COMMITTER_DATE#* }
    # If the first char of the GIT_COMMITTER_TIME_ZONE is "@",
    # it means that no time zone has been parsed. In this case the default will be +0000.
    FIRST_CHAR="$(echo $GIT_COMMITTER_TIME_ZONE | cut -c1-1)"
    if [ "$FIRST_CHAR" = "@" ]; then
        GIT_COMMITTER_TIME_ZONE="+0000"
    fi
    echo "* GIT_COMMITTER_TIME_ZONE = $GIT_COMMITTER_TIME_ZONE"

    #WRONG_RFC_5322_DATE=$(date -d $GIT_COMMITTER_UNIX_DATE -R)
    #echo "* WRONG_RFC_5322_DATE = $WRONG_RFC_5322_DATE"

    RIGHT_RFC_5322_DATE=$(date -d $GIT_COMMITTER_UNIX_DATE +"%a, %d %b %Y")" 00:00:00 $GIT_COMMITTER_TIME_ZONE"
    echo "* RIGHT_RFC_5322_DATE = $RIGHT_RFC_5322_DATE"

    export GIT_COMMITTER_DATE="$RIGHT_RFC_5322_DATE"

    echo "\n"
    echo "-> new GIT_COMMITTER_DATE = $RIGHT_RFC_5322_DATE"
    echo "\n"

    ###########################################################################

    unset LC_ALL
    '
