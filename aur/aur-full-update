#!/bin/sh

# This script will update all the installed AUR packages located in $AUR_HOME

# ------------------------------------------------------------------------------
# Sh strict mode (saner programming env):

# http://redsymbol.net/articles/unofficial-bash-strict-mode/
# https://www.davidpashley.com/articles/writing-robust-shell-scripts/

# exit if any command has a non-zero exit status:
set -o errexit
# same a "set -e"

# exit if a reference to any variable haven't been previously defined:
# (with the exceptions of $* and $@)
set -o nounset
# same as "set -u"

# prevents errors in a pipeline from being masked by returning its error code:
#set -o pipefail

# prevents from overwriting existing files with the > operator
set -o noclobber

# ------------------------------------------------------------------------------
# Script

check_args()
{
    if [ -n "$*" ]
    then
        echo "$@"
        echo "Error - No parameter expected!"
        exit 1
    fi
}

check_env_var()
{
    # https://stackoverflow.com/questions/3601515/how-to-check-if-a-variable-is-set-in-bash#13864829
    if [ -z ${AUR_HOME+x} ]
    then
        echo "Error - the AUR_HOME env variable has not been found!"
        exit 2
    else
        echo "Info - AUR_HOME env variable: $AUR_HOME"
    fi
}

fullupdate()
{
    for dir in "$AUR_HOME"/*
    do
        echo ""
        echo "Info - updating $dir..."
        cd "$dir"
        git_pull_output=$(git pull)
        if [ "$git_pull_output" = "Already up to date." ]
        then
            echo "       Already up to date."
        else
            echo "       Updated, now building..."
            makepkg -is --noconfirm --skippgpcheck
        fi
    done
}


main()
{
    echo "Info - sudo privileges are needed..."
    sudo echo "Info - sudo privileges are granted!"
    check_args "$@"
    check_env_var
    fullupdate
}

main "$@"

