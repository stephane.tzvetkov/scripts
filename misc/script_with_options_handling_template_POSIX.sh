#!/bin/sh

# references:
# https://stackoverflow.com/questions/192249/how-do-i-parse-command-line-arguments-in-bash#192266
# https://gist.github.com/bobpaul/ecd74cdf7681516703f20726431eaceb
# http://abhipandey.com/2016/03/getopt-vs-getopts/
# https://www.etalabs.net/sh_tricks.html

# =================================================================================================
# PROGRAMMING ENV
# =================================================================================================
# Sh strict mode (saner programming env):

# http://redsymbol.net/articles/unofficial-bash-strict-mode/
# https://www.davidpashley.com/articles/writing-robust-shell-scripts/

# exit if any command has a non-zero exit status:
set -o errexit
# same a "set -e"

# exit if a reference to any variable haven't been previously defined:
# (with the exceptions of $* and $@)
set -o nounset
# same as "set -u"

# prevents from overwriting existing files with the > operator
set -o noclobber

# debug mode
#set -x 

# =================================================================================================
# INITIALIZE OPTIONS
# =================================================================================================

# An opt followed by a single colon ':' means that it *needs* an argument
# An opt followed by double colons '::' means that its argument is optional
# Getopts function can be used to parse long options by putting a dash character followed by a colon
OPTS=':dfhl:m:o:p:ki:s:u:v-:'

debug=false 					# d
force=false 					# f
help=false 						# h
loglevel=0 						# l:
mkdir="/home/$USER/test"		# m:
outfile="test"					# o:
password=""						# p:
okok=false 						# k
server_ip="127.0.0.1"			# i:
status="down"					# s:
username="$USER"				# u:
verbose=false 					# v

USAGE="

Scriptname:
	Short script description.

Usage:
	$(basename "$0") [-d | --debug] [-f | --force] [-v | --verbose] [-l | loglevel] (-k | --okok) [-u username] [-p password] [-i server_name_or_IP] 
	$(basename "$0") [-dflkv] (-m /path/to/mkdir) (-o outfile)
	$(basename "$0") (-h | --help)
	$(basename "$0") (-s | --status)
	...

Options:
	-h  show this help text
	-m  path to mkdir
	-k  print 'ok ok'
	-p  password (will be prompted otherwise)
	-i  server ip address (default to '127.0.0.1')
	...

Arguments:
	<plop> description... (defaults to 'test')
	...

Examples:
	# print 'ok ok':
	$(basename "$0") (-k | --help)

	...
"

# =================================================================================================
# HANDLE OPTIONS
# =================================================================================================

while getopts $OPTS optchar; do
	case "$optchar" in

		# Long options (string after a '--'):
		-)
			case "${OPTARG}" in
				debug)
					echo "Parsing --debug option..."
					debug=true
					;;
				force)
					echo "Parsing --force option..."
					force=true
					;;
				help)
					echo "Parsing --help option...'"
					help=true
					;;
				loglevel)
					echo "Parsing --loglevel option: '${OPTARG}'..."
					loglevel=$OPTARG
					;;
				mkdir)
					echo "Parsing --mkdir option: '${OPTARG}'..."
					mkdir=$OPTARG
					;;
				outfile)
					echo "Parsing --outfile option: '${OPTARG}'..."
					outfile=$OPTARG
					;;
				password)
					echo "Parsing --password option: '${OPTARG}'..."
					password=$OPTARG
					;;
				okok)
					echo "Parsing --okok option..."
					okok=true
					;;
				serverip)
					echo "Parsing --servip option: '${OPTARG}'..."
					server_ip=$OPTARG
					;;
				status)
					echo "Parsing --status option: '${OPTARG}'..."
					status=$OPTARG
					;;
				username)
					echo "Parsing --username option: '${OPTARG}'..."
					username=$OPTARG
					;;
				verbose)
					echo "Parsing --verbose option..."
					verbose=true
					;;

				# Long options error handeling
				:) 
					printf "Missing argument for -%s\n" "$OPTARG" >&2
					echo "$USAGE" >&2
					exit 1
					;;
				\?)
					printf "Illegal option: -%s\n" "$OPTARG" >&2
					echo "$USAGE" >&2
					exit 1
					;;
				*)
					echo "Unknown option --${OPTARG}" >&2
					exit 1
					;;
			esac;;

		# Short options (char after a '-):
		d)
			echo "Parsing -d option..."
			debug=true
			;;
		f)
			echo "Parsing -f option...'"
			force=true
			;;
		h)
			echo "Parsing -h option...'"
			help=true
			;;
		l)
			echo "Parsing -l option: '${OPTARG}'..."
			loglevel=$OPTARG
			;;
		m)
			echo "Parsing -m option: '${OPTARG}'..."
			mkdir=$OPTARG
			;;
		o)
			echo "Parsing -o option: '${OPTARG}'..."
			outfile=$OPTARG
			;;
		p)
			echo "Parsing -p option: '${OPTARG}'..."
			password=$OPTARG
			;;
		k)
			echo "Parsing -k option..."
			okok=true
			;;
		i)
			echo "Parsing -i option: '${OPTARG}'..."
			server_ip=$OPTARG
			;;
		s)
			echo "Parsing -s option: '${OPTARG}'..."
			status=$OPTARG
			;;
		u)
			echo "Parsing -u option: '${OPTARG}'..."
			username=$OPTARG
			;;
		v)
			echo "Parsing -v option..."
			verbose=true
			;;

		# Short options errors handeling
		:)
			printf "Missing argument for -%s\n" "$OPTARG" >&2
			echo "$USAGE" >&2
			exit 1
			;;
		\?)
			printf "Illegal option: -%s\n" "$OPTARG" >&2
			echo "$USAGE" >&2
			exit 1
			;;
		*)
			echo "Unknown option --${OPTARG}" >&2
			exit 1
			;;
	esac
done

# Access remaining args
shift $((OPTIND - 1))
if [ "$#" -ne 0 ]
then

	echo ""
	echo "Parsing remaining args... ($*)"

	for arg in "$@"; do
		  echo "do something with $arg"
	done
	echo ""
fi

# =================================================================================================
# FONCTIONS
# =================================================================================================

# Safe POSIX compliant password asking:
get_password () {
	stty -echo
	printf "Password: "
	read -r password
	stty echo
	printf "\n"
}

# =================================================================================================
# SCRIPT PROCESSING
# =================================================================================================

# One condition per usage (cf. $USAGE):

if [ $help = true ]
then
	echo "$USAGE"

elif [ "$status" != "down" ]
then
	echo "status";

else
	if [ $okok = true ]
	then
		echo "ok ok"
	fi
	
	if [ $debug = true ]
	then
		echo "debug"
	fi
	
	if [ $force = true ]
	then
		echo "force"
	fi
	
	if [ $verbose = true ]
	then
		echo "verbose"
		echo "loglevel: $loglevel"
	fi

	# get credentials if not passed as option:
	if [ -z "$password" ]
	then
		get_password
	fi

	# execute commands
	echo "mkdir (not for real) $mkdir"
	echo "server ip is: $server_ip"
	echo "user name is $username"
	echo "outfile is $outfile"

fi

