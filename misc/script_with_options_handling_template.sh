#!/bin/bash

# references:
# https://stackoverflow.com/questions/192249/how-do-i-parse-command-line-arguments-in-bash#192266
# https://gist.github.com/bobpaul/ecd74cdf7681516703f20726431eaceb

# ------------------------------------------------------------------------------
# Sh strict mode (saner programming env):

# http://redsymbol.net/articles/unofficial-bash-strict-mode/
# https://www.davidpashley.com/articles/writing-robust-shell-scripts/

# exit if any command has a non-zero exit status:
set -o errexit
# same a "set -e"

# exit if a reference to any variable haven't been previously defined:
# (with the exceptions of $* and $@)
#set -o nounset
# same as "set -u"

# prevents errors in a pipeline from being masked by returning its error code:
set -o pipefail

# prevents from overwriting existing files with the > operator
set -o noclobber

# ------------------------------------------------------------------------------
# Initialize params variables

# An opt followed by a single colon ':' means that it *needs* an argument
# An opt followed by double colons '::' means that its argument is optional
OPTS=dfhko:sv
LONGOPTS=debug,force,help,kill,output:,status,verbose

DEBUG=0
FORCE=0
HELP=0
KILL=0
OUTFILE=""
STATUS=0
VERBOSE=0

declare -a ARGS

# ------------------------------------------------------------------------------
# Options handling

getopt_check () {
	! getopt --test > /dev/null
	pipestatus=${pipestatus[1]}
	if [[ $pipestatus -ne 4 ]]
	then
	    echo "Error - 'getopt --test' failed in this environment"
		echo "        GNU's enhanced getopt is required to run this script"
	    exit 1
	fi
}

stdout_check () {
	pipestatus=${pipestatus[1]}
	if [[ $pipestatus -ne 0 ]]
	then
	    # e.g. return value is 1
		echo "Error - getop complains about wrong arguments to stdout"
	    exit 2
	fi
}

options () {
	#getopt_check

	# -use ! and PIPESTATUS to get exit code with errexit set
	# -temporarily store output to be able to check for errors
	# -activate quoting/enhanced mode (e.g. by writing out “--options”)
	# -pass arguments only via   -- "$@"   to separate them correctly
	# - getopt auto adds "--" at the end of ${PARSED}, wich is later set to
	#   "$@" using the set command
	! PARSED=$(getopt --options=$OPTS \
					  --longoptions=$LONGOPTS \
					  --name "$0" \
					  -- "$@")
	stdout_check

	# use eval with "$PARSED" to properly handle the quoting
	# the set command sets the list of arguments equal to ${PARSED}
	eval set -- "$PARSED"

	while [[ $# -gt 0 ]]
	do
	    case "$1" in
	        -d|--debug)
	            DEBUG=1
				shift
	            ;;
	        -f|--force)
	            FORCE=1
				shift
	            ;;
			-h|--help)
				HELP=1
				shift
				;;
			-k|--kill)
				KILL=1
				shift
				;;
			-o|--output)
				OUTFILE="$2"
				shift 2
				;;
			-s|--status)
				STATUS=1
				shift
				;;
	        -v|--verbose)
	            VERBOSE=1
				shift
	            ;;
	        --)
				# parsing extra non opts args
				for arg in "$@"
				do
					if [ "$arg" != "--" ]
					then
						ARGS+=("$arg");
						echo "arg: $arg"
					fi
				done

				shift
				break
	            ;;
	        *)
				echo "Error - Args parsing went wrong!"
				exit 3
	            ;;
	    esac
	done
}

args_nb_check () {
	# handle non-option arguments
	if [[ ${#ARGS[@]} -ne 2 ]]
	then
		echo "Error - Illegal number of parameters:"
		echo "        you need a first parameter to specify a root command to kill!"
		echo "        you need a second parameter to specify the full command!"
	    exit 4
	fi
}

# ------------------------------------------------------------------------------
# Script

statuscmd () {
	echo "do something here..."
}

killcmd () {
	echo "do something here..."
}

main () {
	options "$@"
	echo "args: ${ARGS[*]}"
	args_nb_check "${ARGS[*]}"

	if [ $STATUS -eq 1 ]
	then
		statuscmd "${ARGS[*]}"
	fi

	if [ $KILL -eq 1 ]
	then
		killcmd "${ARGS[*]}"
	fi

	echo "plop"
}

main "$@"

